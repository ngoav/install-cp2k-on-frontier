# Install CP2K on Frontier

## Getting started

Feb 2, 2024

This document sumarizes steps and few problems, which I encountered when installing CP2K on Frontier at ORNL.

The orignal built instruction is here: https://github.com/cp2k/cp2k/blob/master/INSTALL.md

## Let's Clone


```
git clone --recursive https://github.com/cp2k/cp2k.git cp2k
```

## Test and Deploy

[I will update this once some calculations are done]

## Installation with OpenMPI
```
cd cp2k/tools/toolchain
module purge
module load cmake
module load openblas
module load ums/default ums002/default openmpi/4.1.5
module load netlib-scalapack/2.2.0
module load DefApps/default gsl/2.7.1

./install_cp2k_toolchain.sh  --with-openblas=system --with-fftw=install --enable-hip=yes \
 --gpu-ver=Mi250 --with-cmake=system --mpi-mode=openmpi --math-mode=openblas --enable-cray=yes \
  --with-gcc=system --with-cmake=system --with-openmpi=system --with-acml=no --with-mkl=no \
  --with-cosma=no --with-scalapack=system --with-openblas=system --with-ptscotch=no --with-superlu=no \
   --with-pexsi=no --with-gsl=system --with-hdf5=install --with-elpa=no --with-libxc=install \
    --with-libint=install --with-spglib=install --with-sirius=no --with-libvdwxc=install \
     --with-spfft=install --with-libxsmm=no --with-libvori=no
``` 

then,
```
cp install/arch/* ../../arch
cd ../..
```
then edit this file using vi:
```
vi arch/local_hip.ssmp; # ssmp means only openmpi
# add this to the end of the right-hand side of DFLAGS: 

-D__NO_OFFLOAD_GRID  -D__NO_OFFLOAD_DBM -D__NO_OFFLOAD_PW -D__OFFLOAD_UNIFIED_MEMORY

# Add this to LDFLAGS, right after –enable-new-dtags: 

,--copy-dt-needed-entries

# this flag, --copy-dt-needed-entries is to suppress a complain about linkage for hdf5 library \
# due to a newer CMAKE version.
```

This editing can be done easier with `sed`:

```
sed -i -e 's/DFLAGS      =/DFLAGS      =  -D__NO_OFFLOAD_GRID  -D__NO_OFFLOAD_DBM -D__NO_OFFLOAD_PW -D__OFFLOAD_UNIFIED_MEMORY/' \
    -e 's/--enable-new-dtags/--enable-new-dtags,--copy-dt-needed-entries/' arch/local_hip.ssmp
```

Finally, you can run make in the main folder, cp2k:

```
source tools/toolchain/install/setup

make -j 128 ARCH=local_hip VERSION=ssmp
```

## Installation with MPICH

### OpenMPI vs MPICH

A previous version of the compilation receipe used the openmpi/4.1.5 module to compile CP2K. Using this to run cp2k.ssmp (single node and shared parrallelism without MPI) is fine. But, attempting to run cp2k.pspm (multiple nodes with MPI) resulted in the following error during runtime:

```
The application appears to have been direct launched using "srun",
but OMPI was not built with SLURM support. This usually happens
when OMPI was not configured --with-slurm and we weren't able
to discover a SLURM installation in the usual places.
 
Please configure as appropriate and try again.
```

This indicates that the openmpi module is built without slurm (confirmed by Nrushad Joshi from OLCF). Besides, MPICH is the prefered MPI implementation in Frontier. So, here I used the PrgEnv-gnu, which includes cray-mpich/8.1.2 within it.

```
cd cp2k/tools/toolchain
module purge
module load PrgEnv-gnu Core/24.07 openblas/0.3.26-omp gsl/2.7.1 rocm netlib-scalapack/2.2.0
```

However, CP2K complains that:

1) mpiexec is not found
2) -lmpicxx is not found

For the first one, I create a script calles $HOME/bin/mpiexec that redirects to srun. And for the second, I replaces all instances of -lmpicxx in the CP2K install script to -lmpi_gnu_123 since all functionalities are in this library (solution from Subil Abraham at OLCF).

```

echo -e '#!/bin/bash\nsrun $@' > $HOME/bin/mpiexec
chmod +x ~/bin/mpiexec
sed -i 's/-lmpicxx/-lmpi_gnu_123/' scripts/stage1/install_mpich.sh

```

We can now run the install script:

```
./install_cp2k_toolchain.sh  --with-openblas=system --with-fftw=install --enable-hip=yes \
 --gpu-ver=Mi250 --with-cmake=system --mpi-mode=mpich --math-mode=openblas --enable-cray=yes \
  --with-gcc=system --with-cmake=system --with-mpich=system --with-acml=no --with-mkl=no \
  --with-cosma=no --with-scalapack=system --with-openblas=system --with-gsl=system --with-hdf5=install --with-elpa=no --with-libxc=install \
    --with-libint=install --with-spglib=install --with-sirius=no --with-libvdwxc=install \
     --with-spfft=install --with-libxsmm=no --with-libvori=no
```

Then we edit local_hip.psmp:

```
sed -i -e 's/DFLAGS      =/DFLAGS      =  -D__NO_OFFLOAD_GRID  -D__NO_OFFLOAD_DBM -D__NO_OFFLOAD_PW -D__OFFLOAD_UNIFIED_MEMORY/' \
    -e 's/--enable-new-dtags/--enable-new-dtags,--copy-dt-needed-entries/' arch/local_hip.psmp
```

Finally, you can run make in the main folder as per the previous strategy.=:

```
source tools/toolchain/install/setup

make -j 128 ARCH=local_hip VERSION=psmp
```

## Usage
----
Now, the executables cp2k.ssmp and cp2k.psmp should be in exe/local_hip

Adding to your PATH
```
export PATH="your_path/cp2k/exe/local_hip":$PATH
cp2k.ssmp your_input; # a single node

#Multinodes
srun -N$N cp2k.psmp your_input; # this should be run with a sbatch (only on the mpich installation method)
```

## Contributing
Bharath Raghavan (postdoc) @ORNL contributed to this installation.

Van Ngo (staff scientist) @ORNL contributed to this installation.

Problems or questions, send to ngoav @@@@@@ ornl.gov

## Problems
I encountered some problems with the pre-installed libraries of openblas, fftw3, hdf5 on Frontier. After fixing the linking errors with --copy-dt-needed-entries with the freshly installed libraries, I haven't tested for these pre-installed libraries. 

I think these libraries should work as they should, so in the install_cp2k_toolchain.sh command, change the options for these libraries from "install" to "system". If these do not work, you can always use "install" option to build all neccessary libaries.

Update from Bharath:

I tried to use some of the default libraries, here are my results:

### FFTW3

Frontier provides the cray-fftw/3.3.10.9 for FFTW3 (ml craype-x86-genoa cray-fftw/3.3.10.9). But using this during CP2K compilation results in: FFTW3 is missing or incomplete error by the libvdwxc installer required by CP2K.

### HDF5

Using the HDF5 module leads to linker errors during CP2K compilation.


## License
Copyrights@ORNL if any

## Project status
For MPICH, we have a built recipe for MiMIC/CP2K/Gromacs to run QM/MM simulations here, https://gitlab.com/bharurn/compiling-mimic-cp2k-on-frontier.

